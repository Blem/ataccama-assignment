# About

This repository contains a simple backend application for a web based database browser. The current version of application supports only __MySql__ database.

## Testing

Integration tests assume that an instance of MySql with specific databases and database users is available on `http://localhost:3306`.
The easiest way how to deploy the configured MySql is to use a Docker configuration provided with the sources.
(The tests require empty databases!)  

```bash
# Start service dependencies if they are not running already
docker-compose -f test_db/docker-compose.yml up -d

#Run tests
./gradlew test

# Stop service
docker-compose -f test_db/docker-compose.yml down
```

Otherwise, you can execute `test_db/01_create_users_and_databases.sql` in your local MySql instance to setup the test environment.

## Basic usage
```bash
# Start service dependencies if they are not running already
docker-compose -f test_db/docker-compose.yml up -d

#Run tests
./gradlew bootRun
```

(For production, it is recommended to not use the test database setup!)

## REST API

### Connection details

```HTTP
GET /api/connections
```
* returns a list of available connection details

```HTTP
GET /api/connections/{name}
```
* returns a single connection detail with the given name

```HTTP
POST /api/connections
```
* inserts a new connection detail

```HTTP
PUT /api/connections/{name}
```
* updates a connection detail with the given name

```HTTP
DELETE /api/connections/{name}
```
* removes a connection detail with the given name

Request body example (for `PUT` and `POST` endpoints):
```json
{
    "name": "example",
    "hostname": "localhost",
    "port": 3306,
    "databaseName": "example",
    "username": "dummyuser",
    "password": "password456"
}
```

### Inspections

```HTTP
GET /api/connections/{connection name}/tables
```
* returns a list of tables in the database
* example:
```json
[
    {
        "name": "departments",
        "type": "TABLE",
        "remarks": ""
    },
    {
        "name": "employees",
        "type": "TABLE",
        "remarks": ""
    },
    //...
]
```

```HTTP
GET /api/connections/{connection name}/tables/{table name}/columns
```
* returns a list of columns in the given table
* example:
```json
[    
    {
        "name": "emp_no",
        "type": "INT",
        "size": 10,
        "decimalDigits": 0,
        "isNullable": false,
        "remarks": "",
        "defaultValue": null,
        "charOctetLength": 0,
        "index": 1,
        "isGeneratedColumn": false,
        "isPrimaryKey": true,
        "primaryKeyIndex": 1,
        "primaryKeyName": "PRIMARY",
        "autoIncrement": false
    },
    {
        "name": "first_name",
        "type": "VARCHAR",
        "size": 14,
        "decimalDigits": 0,
        "isNullable": false,
        "remarks": "",
        "defaultValue": null,
        "charOctetLength": 14,
        "index": 3,
        "isGeneratedColumn": false,
        "isPrimaryKey": false,
        "primaryKeyIndex": null,
        "primaryKeyName": null,
        "autoIncrement": false
    },

    //...
]
```

```HTTP
GET /api/connections/{connection name}/{table name}/preview[?limit=(positive integer)]
```
* returns a sample of data in the table
* example:
```json
[
    {
        "last_name": "Facello",
        "hire_date": "1986-06-26",
        "gender": "M",
        "emp_no": 10001,
        "first_name": "Georgi",
        "birth_date": "1953-09-02"
    },
    {
        "last_name": "Simmel",
        "hire_date": "1985-11-21",
        "gender": "F",
        "emp_no": 10002,
        "first_name": "Bezalel",
        "birth_date": "1964-06-02"
    }

    //...
]
```

### Statistics

```HTTP
GET /api/connections/{connection name}/tables/{table name}/statistics
```
* returns simple statistics for the given table
* example:
```json
{
    "numberOfRows": 60,
    "numberOfColumns": 6
}
```

```HTTP
GET /api/connections/{connection name}/tables/{table name}/columns/{column name}/statistics
```
* returns simple statistics for the given column
* example:
```json
{
    "min": "1985-02-18",
    "max": "1999-04-30",
    "avg": 19899460.55 // average has no meaning for dates
}
```