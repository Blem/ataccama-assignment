package org.bitbucket.blem.ataccamadb

import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.cfg.MapperConfig
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod
import org.bitbucket.blem.ataccamadb.controllers.ConnectionsController
import org.bitbucket.blem.ataccamadb.controllers.InspectionController
import org.bitbucket.blem.ataccamadb.controllers.StatisticsController
import org.bitbucket.blem.ataccamadb.data.ConnectionsDAO
import org.bitbucket.blem.ataccamadb.data.MySqlConnectionsDAOImpl
import org.bitbucket.blem.ataccamadb.services.InspectionService
import org.bitbucket.blem.ataccamadb.services.StatisticsService
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.jdbc.core.JdbcTemplate

@SpringBootConfiguration
@EnableAutoConfiguration
class AtaccamaDbApplication {
    @Bean
    fun connectionsDAO(jdbcTemplate: JdbcTemplate): ConnectionsDAO = MySqlConnectionsDAOImpl(jdbcTemplate)

    @Bean
    fun connectionsController(connectionsDAO: ConnectionsDAO) = ConnectionsController(connectionsDAO)

    @Bean
    fun inspectionService(connectionsDAO: ConnectionsDAO) = InspectionService(connectionsDAO)

    @Bean
    fun inspectionController(inspectionService: InspectionService) = InspectionController(inspectionService)

    @Bean
    fun statisticsService(connectionsDAO: ConnectionsDAO) = StatisticsService(connectionsDAO)

    @Bean
    fun statisticsController(statisticsService: StatisticsService) = StatisticsController(statisticsService)

    @Bean
    fun jacksonNamingConvection() = Jackson2ObjectMapperBuilderCustomizer {
        it.propertyNamingStrategy(object : PropertyNamingStrategy() {
            override fun nameForGetterMethod(config: MapperConfig<*>, method: AnnotatedMethod, defaultName: String): String {
                if (method.hasReturnType() && method.rawReturnType.kotlin == Boolean::class && method.name.startsWith("is")) {
                    return method.name
                }

                return super.nameForGetterMethod(config, method, defaultName)
            }
        })
    }
}

fun main(args: Array<String>) {
    runApplication<AtaccamaDbApplication>(*args)
}
