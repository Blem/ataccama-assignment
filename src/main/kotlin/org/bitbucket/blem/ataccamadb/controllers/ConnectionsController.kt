package org.bitbucket.blem.ataccamadb.controllers

import org.bitbucket.blem.ataccamadb.data.ConnectionsDAO
import org.bitbucket.blem.ataccamadb.models.ConnectionInfo
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

/**
 * Controller used to manage the connections repository.
 */
@RestController
@RequestMapping("/api/connections")
class ConnectionsController(private val repository: ConnectionsDAO) {
    //TODO: Add validation?

    @GetMapping
    fun getAllConnections() = repository.findAll()

    @GetMapping("/{name}")
    fun getSingleConnection(@PathVariable name: String) = repository.find(name)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun insertConnection(@RequestBody connection: ConnectionInfo) {
        repository.insert(connection)
    }

    @PutMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun updateConnection(@PathVariable name: String, @RequestBody connection: ConnectionInfo) {
        if (!repository.update(name, connection)) throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteConnection(@PathVariable name: String) {
        if (!repository.delete(name)) throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}