package org.bitbucket.blem.ataccamadb.controllers

import org.bitbucket.blem.ataccamadb.services.InspectionService
import org.springframework.web.bind.annotation.*

/**
 * Controller used to provide a database inspections.
 */
@RestController
@RequestMapping("/api/connections/{connectionName}/tables")
class InspectionController(private val inspectionService: InspectionService) {

    @GetMapping
    fun getTables(@PathVariable connectionName: String) = withExceptionHandler {
        inspectionService.getTables(connectionName)
    }

    @GetMapping("/{tableName}/columns")
    fun getTableColumns(@PathVariable connectionName: String, @PathVariable tableName: String) = withExceptionHandler {
        inspectionService.getColumns(connectionName, tableName)
    }

    @GetMapping("/{tableName}/preview")
    fun getTablePreview(@PathVariable connectionName: String, @PathVariable tableName: String, @RequestParam limit: Int?) = withExceptionHandler {
        inspectionService.getPreview(connectionName, tableName, limit ?: 0)
    }
}