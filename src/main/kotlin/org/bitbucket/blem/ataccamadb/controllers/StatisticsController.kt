package org.bitbucket.blem.ataccamadb.controllers

import org.bitbucket.blem.ataccamadb.services.StatisticsService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/connections/{connectionName}/tables/{tableName}")
class StatisticsController(private val statisticsService: StatisticsService) {

    @GetMapping("/statistics")
    fun getTableStatistics(@PathVariable connectionName: String, @PathVariable tableName: String) = withExceptionHandler {
        statisticsService.getTableStatistics(connectionName, tableName)
    }

    @GetMapping("/columns/{columnName}/statistics")
    fun getColumnStatistics(@PathVariable connectionName: String, @PathVariable tableName: String, @PathVariable columnName: String) = withExceptionHandler {
        statisticsService.getColumnStatistics(connectionName, tableName, columnName)
    }
}