package org.bitbucket.blem.ataccamadb.controllers

import org.bitbucket.blem.ataccamadb.services.ConnectionNotFoundException
import org.bitbucket.blem.ataccamadb.services.NotExistsException
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

// it generates better error messages than simple @ExceptionHandler, but... fixme
internal fun <R> withExceptionHandler(block: () -> R): R {
    try {
        return block()
    } catch (e: Exception) {
        when (e) {
            is ConnectionNotFoundException, is NotExistsException -> throw ResponseStatusException(HttpStatus.NOT_FOUND, e.message, e)
            else -> throw e
        }
    }
}