package org.bitbucket.blem.ataccamadb.data

import org.bitbucket.blem.ataccamadb.models.ConnectionInfo

/**
 * Interface specifying a set of operations with a connection repository.
 */
interface ConnectionsDAO {

    /**
     * Finds a connection with the given name in the repository.
     *
     * @return a connection info if exits, otherwise return null
     */
    fun find(name: String): ConnectionInfo?

    /**
     * Finds all connections stored in the repository.
     */
    fun findAll(): List<ConnectionInfo>

    /**
     * Inserts a connection info into repository.
     */
    fun insert(connection: ConnectionInfo)

    /**
     * Updates a connection info with the given name.
     */
    fun update(name: String, connection: ConnectionInfo): Boolean

    /**
     * Removes a connection with the given name from repository.
     */
    fun delete(name: String): Boolean
}