package org.bitbucket.blem.ataccamadb.data

import org.bitbucket.blem.ataccamadb.models.ConnectionInfo
import org.springframework.dao.IncorrectResultSizeDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet

private const val CONNECTIONS_TABLE_NAME = "connections"
private const val KEY_COLUMN_NAME = "name"
private const val SELECT_ONE_SQL_STRING = "SELECT * FROM $CONNECTIONS_TABLE_NAME WHERE $KEY_COLUMN_NAME = ?"
private const val SELECT_ALL_SQL_STRING = "SELECT * FROM $CONNECTIONS_TABLE_NAME"
private const val INSERT_ONE_SQL_STRING = "INSERT INTO $CONNECTIONS_TABLE_NAME (`name`,`hostname`,`port`,`database_name`,`username`,`password`) VALUES (?,?,?,?,?,?);"
private const val UPDATE_SQL_STRING = "UPDATE $CONNECTIONS_TABLE_NAME SET `name` = ?,`hostname` = ?,`port` = ?,`database_name` = ?,`username` = ?,`password` = ? WHERE $KEY_COLUMN_NAME = ?"
private const val DELETE_ONE_SQL_STRING = "DELETE FROM $CONNECTIONS_TABLE_NAME WHERE $KEY_COLUMN_NAME = ?"


/**
 * Provides access into a MySql database that store the connections.
 */
@Repository
class MySqlConnectionsDAOImpl(private val jdbcTemplate: JdbcTemplate) : ConnectionsDAO {

    private fun mapper(rs: ResultSet, n: Int) = ConnectionInfo(
            rs.getString("name"),
            rs.getString("hostname"),
            rs.getInt("port"),
            rs.getString("database_name"),
            rs.getString("username"),
            rs.getString("password")
    )


    override fun find(name: String): ConnectionInfo? {
        return try {
            jdbcTemplate.queryForObject(
                    SELECT_ONE_SQL_STRING,
                    arrayOf(name),
                    ::mapper
            )
        } catch (e: IncorrectResultSizeDataAccessException) {
            null
        }
    }

    override fun findAll() = jdbcTemplate.query(SELECT_ALL_SQL_STRING, ::mapper)

    override fun insert(connection: ConnectionInfo) {
        jdbcTemplate.update(
                INSERT_ONE_SQL_STRING,
                connection.name,
                connection.hostname,
                connection.port,
                connection.databaseName,
                connection.username,
                connection.password
        )
    }

    override fun update(name: String, connection: ConnectionInfo): Boolean {
        val numOfAffectedRows = jdbcTemplate.update(
                UPDATE_SQL_STRING,
                connection.name,
                connection.hostname,
                connection.port,
                connection.databaseName,
                connection.username,
                connection.password,
                name
        )
        return numOfAffectedRows == 1
    }

    override fun delete(name: String): Boolean {
        val numOfAffectedRows = jdbcTemplate.update(DELETE_ONE_SQL_STRING, name)
        return numOfAffectedRows == 1
    }
}