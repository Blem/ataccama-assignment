package org.bitbucket.blem.ataccamadb.models

data class ColumnInfo(
        val name: String,
        val type: String,
        val size: Int,
        val decimalDigits: Int?,
        val isNullable: Boolean?, /* null if the nullability is unknown */
        val remarks: String?,
        val defaultValue: String?,
        val charOctetLength: Int,
        val index: Int,
        val isAutoIncrement: Boolean?,
        val isGeneratedColumn: Boolean?,

        val isPrimaryKey: Boolean,
        val primaryKeyIndex: Short?,
        val primaryKeyName: String?
)