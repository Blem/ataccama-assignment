package org.bitbucket.blem.ataccamadb.models

data class ColumnStatistics(
        val min: Any?,
        val max: Any?,
        val avg: Any?
)