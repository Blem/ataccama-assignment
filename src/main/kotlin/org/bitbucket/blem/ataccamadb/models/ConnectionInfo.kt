package org.bitbucket.blem.ataccamadb.models

data class ConnectionInfo(
        val name: String,
        val hostname: String,
        val port: Int,
        val databaseName: String,
        val username: String,
        val password: String?
)