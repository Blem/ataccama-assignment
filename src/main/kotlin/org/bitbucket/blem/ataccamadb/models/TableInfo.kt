package org.bitbucket.blem.ataccamadb.models

data class TableInfo(
        val name: String,
        val type: String,
        val remarks: String?
)
