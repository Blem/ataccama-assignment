package org.bitbucket.blem.ataccamadb.models

data class TableStatistics(
        val numberOfRows: Int,
        val numberOfColumns: Int
)