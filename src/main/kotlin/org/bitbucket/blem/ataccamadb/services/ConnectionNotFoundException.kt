package org.bitbucket.blem.ataccamadb.services

/**
 * Exception thrown when connection with the given name doesn't exists.
 */
class ConnectionNotFoundException(val connectionName: String, message: String = "Connection with name '$connectionName' not found") : Exception(message)
