package org.bitbucket.blem.ataccamadb.services

import org.bitbucket.blem.ataccamadb.data.ConnectionsDAO
import org.bitbucket.blem.ataccamadb.models.ColumnInfo
import org.bitbucket.blem.ataccamadb.models.TableInfo
import org.springframework.stereotype.Service
import java.sql.DatabaseMetaData.*
import java.sql.SQLSyntaxErrorException

/**
 * Service used to inspect database schemas and data of databases stored in the connection repository.
 */
@Service
class InspectionService(private val repository: ConnectionsDAO) {

    /**
     * Gets a list of tables in a database.
     *
     * @param connectionName a name of connection the connection repository
     */
    fun getTables(connectionName: String): List<TableInfo> {
        val connectionInfo = getConnectionInfo(connectionName)

        return getConnection(connectionInfo).use { connection ->
            connection.metaData.getTables(connection.catalog, connection.schema, null, null).use {
                it.asIterable().map { rs ->
                    TableInfo(
                            rs.getString("TABLE_NAME"),
                            rs.getString("TABLE_TYPE"),
                            rs.getString("REMARKS")
                    )
                }
            }
        }
    }

    /**
     * Gets a list of columns in a database.
     *
     * @param connectionName a name of connection the connection repository
     * @param tableName a name of table to inspect
     */
    fun getColumns(connectionName: String, tableName: String): List<ColumnInfo> {
        val connectionInfo = getConnectionInfo(connectionName)

        try {
            return getConnection(connectionInfo).use { connection ->
                val metaData = connection.metaData

                // get columns that are part of primary key
                val primaryKeys = metaData.getPrimaryKeys(connection.catalog, connection.schema, tableName).use { rs ->
                    rs.asIterable().map { it.getString("COLUMN_NAME") to PkInfo(it.getShort("KEY_SEQ"), it.getString("PK_NAME")) }.toMap()
                }

                // get info about columns in the table
                metaData.getColumns(connection.catalog, connection.schema, tableName, null).use {
                    it.asIterable().map { rs ->
                        val name = rs.getString("COLUMN_NAME")
                        val pkInfo = primaryKeys[name]

                        ColumnInfo(
                                name = name,
                                type = rs.getString("TYPE_NAME"),
                                size = rs.getInt("COLUMN_SIZE"),
                                decimalDigits = rs.getInt("DECIMAL_DIGITS"),
                                isNullable = mapNullability(rs.getInt("NULLABLE")),
                                remarks = rs.getString("REMARKS"),
                                defaultValue = rs.getString("COLUMN_DEF"),
                                charOctetLength = rs.getInt("CHAR_OCTET_LENGTH"),
                                index = rs.getInt("ORDINAL_POSITION"),
                                isAutoIncrement = mapYesNoValue(rs.getString("IS_AUTOINCREMENT")),
                                isGeneratedColumn = mapYesNoValue(rs.getString("IS_GENERATEDCOLUMN")),
                                isPrimaryKey = pkInfo != null,
                                primaryKeyIndex = pkInfo?.primaryKeyIndex,
                                primaryKeyName = pkInfo?.primaryKeyName
                        )
                    }
                }
            }
        } catch (e: SQLSyntaxErrorException) {
            // There is probably more reasons to get this exception, but unknown table is the most likely. It need more examination.
            // IT IS PROBABLY VENDOR SPECIFIC!
            throw NotExistsException("Table with name '$tableName' does not exits in the database", e)
        }
    }

    /**
     * Gets a sample data from a database
     *
     * @param connectionName a name of connection the connection repository
     * @param tableName a name of table to inspect
     * @param limit a number of records to return (>= 0 means unlimited)
     */
    fun getPreview(connectionName: String, tableName: String, limit: Int = 0): List<Map<String, Any?>> {
        val connectionInfo = getConnectionInfo(connectionName)

        val sql =
                if (limit > 0)
                    "SELECT * FROM $tableName LIMIT $limit"
                else
                    "SELECT * FROM $tableName"

        try {
            return getConnection(connectionInfo).use { connection ->
                connection.query(sql) { rs ->
                    val names = rs.getColumnNames()
                    rs.asIterable().map { it.getValues(names) }
                }
            }
        } catch (e: SQLSyntaxErrorException) {
            // the only possible reason to get syntax error is unknown table name (if sql string is correct)
            // IT IS PROBABLY VENDOR SPECIFIC!
            throw NotExistsException("Table with name '$tableName' does not exits in the database", e)
        }
    }

    private fun getConnectionInfo(connectionName: String) = repository.find(connectionName)
            ?: throw ConnectionNotFoundException(connectionName)

    private data class PkInfo(val primaryKeyIndex: Short, val primaryKeyName: String?)

    private fun mapNullability(value: Int) = when (value) {
        columnNoNulls -> false
        columnNullable -> true
        columnNullableUnknown -> null
        else -> throw IllegalArgumentException()
    }

    private fun mapYesNoValue(value: String) = when (value) {
        "YES" -> true
        "NO" -> false
        else -> null
    }
}
