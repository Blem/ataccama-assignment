package org.bitbucket.blem.ataccamadb.services

class NotExistsException(message: String, cause: Throwable) : Exception(message, cause)
