package org.bitbucket.blem.ataccamadb.services

import org.bitbucket.blem.ataccamadb.data.ConnectionsDAO
import org.bitbucket.blem.ataccamadb.models.ColumnStatistics
import org.bitbucket.blem.ataccamadb.models.TableStatistics
import org.springframework.stereotype.Service
import java.sql.SQLSyntaxErrorException

private const val MIN_ALIAS = "min"
private const val MAX_ALIAS = "max"
private const val AVG_ALIAS = "avg"
private const val COUNT_ALIAS = "count"

/**
 * Service used to retrieve statistics about database.
 */
@Service
class StatisticsService(private val repository: ConnectionsDAO) {

    /**
     * Gets simple statistics for a column in a table.
     */
    fun getColumnStatistics(connectionName: String, tableName: String, columnName: String): ColumnStatistics? {
        val connectionInfo = getConnectionInfo(connectionName)

        try {
            return getConnection(connectionInfo).use { connection ->
                val sql = "SELECT min($columnName) as $MIN_ALIAS, max($columnName) as $MAX_ALIAS, avg($columnName) as $AVG_ALIAS FROM $tableName"
                connection.querySingle(sql, { ColumnStatistics(it.getObject(MIN_ALIAS), it.getObject(MAX_ALIAS), it.getObject(AVG_ALIAS)) })
            }
        } catch (e: SQLSyntaxErrorException) {
            // how to differentiate the cases...? fixme
            throw NotExistsException("Table with name '$tableName', or column with name '$columnName' does not exits in the database", e)
        }
    }

    /**
     * Get simple statistics for a table.
     */
    fun getTableStatistics(connectionName: String, tableName: String): TableStatistics {
        val connectionInfo = getConnectionInfo(connectionName)

        try {
            getConnection(connectionInfo).use { connection ->
                val rowCountSql = "SELECT count(*) as $COUNT_ALIAS FROM $tableName"
                val numOfRows = connection.querySingle(rowCountSql, { it.getInt(COUNT_ALIAS) }) ?: 0

                val columnCountSql = "SELECT * FROM $tableName LIMIT 0"
                val numOfColumns = connection.query(columnCountSql, { it.metaData.columnCount })

                return TableStatistics(numOfRows, numOfColumns)
            }
        } catch (e: SQLSyntaxErrorException) {
            throw NotExistsException("Table with name '$tableName' does not exits in the database", e)
        }
    }

    private fun getConnectionInfo(connectionName: String) = repository.find(connectionName)
            ?: throw ConnectionNotFoundException(connectionName)
}