package org.bitbucket.blem.ataccamadb.services

import org.bitbucket.blem.ataccamadb.models.ConnectionInfo
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet

internal fun getConnection(connectionInfo: ConnectionInfo): Connection {
    val (_, hostname, port, database, username, password) = connectionInfo
    val connectionString = "jdbc:mysql://$hostname:$port/$database"
    return DriverManager.getConnection(connectionString, username, password)
}

internal fun <R> Connection.query(sql: String, block: (ResultSet) -> R): R {
    return createStatement().use { statement ->
        statement.use { it.executeQuery(sql).use(block) }
    }
}

internal fun <R> Connection.querySingle(sql: String, mapper: (ResultSet) -> R): R? {
    return this.query(sql) { rs ->
        if (rs.next()) {
            val result = mapper(rs)

            if (!rs.next()) {
                result
            } else {
                throw IllegalStateException("Query can select only zero or one result.")
            }
        } else {
            null
        }
    }
}

internal operator fun ResultSet.iterator(): Iterator<ResultSet> {
    val rs = this
    return object : Iterator<ResultSet> {
        override fun hasNext(): Boolean = rs.next()
        override fun next(): ResultSet = rs
    }
}

internal fun ResultSet.asIterable() = Iterable { this.iterator() }

internal fun ResultSet.getColumnNames() = Array(metaData.columnCount, { metaData.getColumnName(it + 1) })

internal fun ResultSet.getValues(columnNames: Array<String>): HashMap<String, Any?> {
    val result = HashMap<String, Any?>(columnNames.size)
    columnNames.forEach { result[it] = this.getObject(it) }
    return result
}