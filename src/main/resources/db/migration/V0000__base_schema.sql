ALTER DATABASE DEFAULT CHAR SET utf8
DEFAULT COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS connections (
    id_connection   INT             NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name            VARCHAR(50)     NOT NULL UNIQUE,
    hostname        VARCHAR(50)     NOT NULL,
    port            INT             NOT NULL,
    databaseName    VARCHAR(50)     NOT NULL,
    username        VARCHAR(50)     NOT NULL,
    password        VARCHAR(100)
);
