import org.bitbucket.blem.ataccamadb.AtaccamaDbApplication
import org.hamcrest.number.IsCloseTo
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.transaction.annotation.Transactional
import java.sql.DriverManager

private const val TEST_INSERT_STATEMENT = "INSERT INTO connections (`name`,`hostname`,`port`,`database_name`,`username`,`password`) VALUES ('$DB_NAME','$DB_HOST',$DB_PORT,'$DB_NAME','$DB_USERNAME','$DB_PASSWORD')"
private const val BASE_URL = "/api/connections/example/tables/employees"

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [AtaccamaDbApplication::class])
@AutoConfigureMockMvc
@Transactional
@Sql(statements = [TEST_INSERT_STATEMENT])
class BonusTaskIntegrationTests(@Autowired private val mvc: MockMvc) {
    private val connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)

    @BeforeAll
    fun beforeAll() {
        connection.executeScripts("test_schema.sql", "test_data.sql")
    }

    @AfterAll
    fun afterAll() {
        connection.executeScripts("test_cleanup.sql")
        connection.close()
    }

    @Test
    fun `get table statistics`() {
        mvc.perform(get("$BASE_URL/statistics"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.numberOfRows").value(60))
                .andExpect(jsonPath("$.numberOfColumns").value(6))
    }

    @Test
    fun `get statistics for unknown table`() {
        mvc.perform(get("/api/connections/example/tables/unknown/statistics"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `get table statistics for unknown connection`() {
        mvc.perform(get("/api/connections/unknown/tables/any/statistics"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `get integer column statistics`() {
        mvc.perform(get("$BASE_URL/columns/emp_no/statistics"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.min").value(10001))
                .andExpect(jsonPath("$.max").value(10060))
                .andExpect(jsonPath("$.avg").value(IsCloseTo(10030.5, 0.5)))
    }

    @Test
    fun `get string column statistics`() {
        mvc.perform(get("$BASE_URL/columns/first_name/statistics"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.min").value("Adamantios"))
                .andExpect(jsonPath("$.max").value("Zvonko"))
//                .andExpect(jsonPath("$.avg").exists()) // average has no meaning for strings
    }

    @Test
    fun `get column column from unknown table`() {
        mvc.perform(get("/api/connections/example/tables/unknown/columns/any/statistics"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `get statistics for unknown column`() {
        mvc.perform(get("$BASE_URL/columns/unknown/statistics"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `get column statistics for unknown connection`() {
        mvc.perform(get("/api/connections/unknown/tables/any/columns/any/statistics"))
                .andExpect(status().isNotFound)
    }
}