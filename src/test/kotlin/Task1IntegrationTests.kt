import org.assertj.core.api.Assertions.assertThat
import org.bitbucket.blem.ataccamadb.AtaccamaDbApplication
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.jdbc.JdbcTestUtils
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.transaction.annotation.Transactional

private const val BASE_URL = "/api/connections"
private const val UNKNOWN_CONNECTION_NAME = "unknown"
private const val TABLE_NAME = "connections"
private const val TEST_INSERT_STATEMENT = "INSERT INTO $TABLE_NAME (`name`,`hostname`,`port`,`database_name`,`username`,`password`) VALUES ('$CONNECTION_NAME','$DB_HOST',$DB_PORT,'$DB_NAME','$DB_USERNAME','$DB_PASSWORD')"

private const val REQUEST_BODY = """
{
    "name": "example",
    "hostname": "localhost",
    "port": 3306,
    "databaseName": "example",
    "username": "dummyuser",
    "password": "password456"
}
"""

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [AtaccamaDbApplication::class])
@Transactional
@AutoConfigureMockMvc
class Task1IntegrationTests(
        @Autowired val mvc: MockMvc,
        @Autowired val jdbcTemplate: JdbcTemplate
) {

    @Test
    fun `save db connection`() {
        val numOfRows = jdbcTemplate.countRowsInTable(TABLE_NAME)

        mvc.perform(
                post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(REQUEST_BODY)
        ).andExpect(status().isCreated)

        assertThat(jdbcTemplate.countRowsInTable(TABLE_NAME)).isEqualTo(numOfRows + 1)
    }

    @Test
    @Sql(statements = [TEST_INSERT_STATEMENT])
    fun `list db connections`() {
        mvc.perform(get(BASE_URL))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$").isNotEmpty)
    }

    @Test
    @Sql(statements = [TEST_INSERT_STATEMENT])
    fun `get single connection`() {
        mvc.perform(get("$BASE_URL/$CONNECTION_NAME"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.name").value(CONNECTION_NAME))
                .andExpect(jsonPath("$.hostname").value(DB_HOST))
                .andExpect(jsonPath("$.port").value(DB_PORT))
                .andExpect(jsonPath("$.databaseName").value(DB_NAME))
                .andExpect(jsonPath("$.username").value(DB_USERNAME))
                .andExpect(jsonPath("$.password").value(DB_PASSWORD))
    }

    @Test
    fun `get unknown connection`() {
        mvc.perform(get("$BASE_URL/$CONNECTION_NAME"))
                .andExpect(status().isNotFound)
    }

    @Test
    @Sql(statements = [TEST_INSERT_STATEMENT])
    fun `update db connection`() {
        val body = """
            {
                "name": "otherdb",
                "hostname": "localhost",
                "port": 3306,
                "databaseName": "otherdb",
                "username": "otheruser"
            }
        """.trimIndent()

        mvc.perform(
                put("$BASE_URL/$CONNECTION_NAME")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body)
        )
                .andExpect(status().isNoContent)

        assertThat(jdbcTemplate.countRowsInTable(TABLE_NAME, "name='$CONNECTION_NAME'")).isZero()
        assertThat(jdbcTemplate.countRowsInTable(TABLE_NAME, "name='otherdb'")).isEqualTo(1)
    }

    @Test
    fun `update unknown db connection`() {
        mvc.perform(
                put("$BASE_URL/$UNKNOWN_CONNECTION_NAME")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(REQUEST_BODY)
        )
                .andExpect(status().isNotFound)
    }

    @Test
    @Sql(statements = [TEST_INSERT_STATEMENT])
    fun `delete db connection`() {
        mvc.perform(delete("$BASE_URL/$CONNECTION_NAME"))
                .andExpect(status().isNoContent)

        assertThat(jdbcTemplate.countRowsInTable(TABLE_NAME, "name = '$CONNECTION_NAME'")).isZero()
    }

    private fun JdbcTemplate.countRowsInTable(tableName: String) =
            JdbcTestUtils.countRowsInTable(this, tableName)

    private fun JdbcTemplate.countRowsInTable(tableName: String, whereClause: String) =
            JdbcTestUtils.countRowsInTableWhere(this, tableName, whereClause)
}