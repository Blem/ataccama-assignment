import org.bitbucket.blem.ataccamadb.AtaccamaDbApplication
import org.hamcrest.Matchers.hasItems
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.transaction.annotation.Transactional
import java.sql.DriverManager

private const val TEST_INSERT_STATEMENT = "INSERT INTO connections (`name`,`hostname`,`port`,`database_name`,`username`,`password`) VALUES ('$CONNECTION_NAME','$DB_HOST',$DB_PORT,'$DB_NAME','$DB_USERNAME','$DB_PASSWORD')"

private const val BASE_URL = "/api/connections/example"
private const val TEST_TABLE_NAME = "employees"

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [AtaccamaDbApplication::class])
@AutoConfigureMockMvc
@Transactional
@Sql(statements = [TEST_INSERT_STATEMENT])
class Task2IntegrationTests(@Autowired private val mvc: MockMvc) {
    private val connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)

    @BeforeAll
    fun beforeAll() {
        connection.executeScripts("test_schema.sql", "test_data.sql")
    }

    @AfterAll
    fun afterAll() {
        connection.executeScripts("test_cleanup.sql")
        connection.close()
    }

    @Test
    fun `list tables`() {
        // /api/connections/{connection name}/tables
        mvc.perform(get("$BASE_URL/tables"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$").isNotEmpty)
                .andExpect(jsonPath("$[*].name").value(hasItems(TEST_TABLE_NAME)))
    }

    @Test
    fun `list tables in unknown db`() {
        mvc.perform(get("/api/connections/unknown/tables"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `list columns`() {
        // /api/connections/{connection name}/tables/{table name}/columns
        mvc.perform(get("$BASE_URL/tables/$TEST_TABLE_NAME/columns"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$").isNotEmpty)
                .andExpect(jsonPath("$[?(@.name == 'emp_no')].isPrimaryKey").value(true))
    }

    @Test
    fun `column info properties`() {
        mvc.perform(get("$BASE_URL/tables/$TEST_TABLE_NAME/columns"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$").isNotEmpty)

                .andExpect(jsonPath("$[0].name").hasJsonPath())
                .andExpect(jsonPath("$[0].type").hasJsonPath())
                .andExpect(jsonPath("$[0].size").hasJsonPath())
                .andExpect(jsonPath("$[0].decimalDigits").hasJsonPath())
                .andExpect(jsonPath("$[0].isNullable").hasJsonPath())
                .andExpect(jsonPath("$[0].remarks").hasJsonPath())
                .andExpect(jsonPath("$[0].defaultValue").hasJsonPath())
                .andExpect(jsonPath("$[0].charOctetLength").hasJsonPath())
                .andExpect(jsonPath("$[0].index").hasJsonPath())
                .andExpect(jsonPath("$[0].isAutoIncrement").hasJsonPath())
                .andExpect(jsonPath("$[0].isGeneratedColumn").hasJsonPath())
                .andExpect(jsonPath("$[0].isPrimaryKey").hasJsonPath())
                .andExpect(jsonPath("$[0].primaryKeyIndex").hasJsonPath())
                .andExpect(jsonPath("$[0].primaryKeyName").hasJsonPath())
    }

    @Test
    fun `list columns in unknown db`() {
        mvc.perform(get("/api/connections/unknown/tables/unknown/columns"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `list columns in unknown table`() {
        mvc.perform(get("$BASE_URL/tables/unknown/columns"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `get preview of a table`() {
        // /api/connections/{connection name}/tables/{table name}/preview
        mvc.perform(get("$BASE_URL/tables/$TEST_TABLE_NAME/preview"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$").isNotEmpty)
                .andExpect(jsonPath("$[*].emp_no").exists())
    }

    @Test
    fun `get limited preview of a table`() {
        val limit = 5
        // /api/connections/{connection name}/tables/{table name}/preview
        mvc.perform(get("$BASE_URL/tables/$TEST_TABLE_NAME/preview?limit=$limit"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$.length()").value(limit))
    }

    @Test
    fun `get preview in unknown db`() {
        mvc.perform(get("/api/connections/unknown/tables/unknown/preview"))
                .andExpect(status().isNotFound)
    }

    @Test
    fun `get preview of unknown table`() {
        mvc.perform(get("$BASE_URL/tables/unknown/preview"))
                .andExpect(status().isNotFound)
    }
}