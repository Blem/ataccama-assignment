import org.springframework.core.io.DefaultResourceLoader
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator
import java.sql.Connection

fun Connection.executeScripts(vararg scripts: String) {
    val loader = DefaultResourceLoader()
    ResourceDatabasePopulator(*scripts.map(loader::getResource).toTypedArray()).populate(this)
}