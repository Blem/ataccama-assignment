-- creates a database for the connections
CREATE USER 'ataccamauser'@'%' IDENTIFIED BY 'password123';
CREATE DATABASE connections_db;
GRANT ALL PRIVILEGES ON connections_db.* TO 'ataccamauser'@'%';


-- creates a test database
CREATE USER 'dummyuser'@'%' IDENTIFIED BY 'password456';
CREATE DATABASE example;
GRANT ALL PRIVILEGES ON example.* TO 'dummyuser'@'%';